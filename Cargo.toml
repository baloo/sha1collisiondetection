[package]
name = "sha1collisiondetection"
description = "SHA-1 hash function with collision detection and mitigation"
readme = "README.md"
version = "0.3.4"
authors = [
    "Marc Stevens <marc@marc-stevens.nl>",
    "Dan Shumow <danshu@microsoft.com>",
    "Justus Winter <justus@sequoia-pgp.org>",
]
edition = "2021"
license = "MIT"
documentation = "https://docs.rs/sha1collisiondetection"
repository = "https://gitlab.com/sequoia-pgp/sha1collisiondetection"
keywords = ["sha1", "hash", "digest", "collision", "mitigation"]
categories = ["cryptography", "no-std"]
rust-version = "1.60"
exclude = [
    "/lib/*.c",
    "/lib/*.h",
    "/src/*.c",
    "/vs2015/",
    "/compile_commands.json",
    "/Makefile",
    "/test-sha1sum-protocol.sh",
    "/*.yml",
]

[badges]
gitlab = { repository = "sequoia-pgp/sha1collisiondetection" }
maintenance = { status = "actively-developed" }

[dependencies]
const-oid = { version = "0.9", default-features = false, optional = true }
generic-array = ">= 0.12, < 2"
digest = { version = "0.10", optional = true }
clap = { version = "4", default-features = false, features = ["std", "derive", "help", "wrap_help"], optional = true }
clap_mangen = { version = "0.2", optional = true, default-features = false }

[build-dependencies]
clap = { version = "4", default-features = false, features = ["derive", "std"], optional = true }
clap_mangen = { version = "0.2", optional = true, default-features = false }

[dev-dependencies]
getrandom = "0.2"
hex-literal = "0.4"
sha1 = "0.10"

[lib]
path = "lib/lib.rs"

[[bin]]
path = "src/main.rs"
name = "sha1cdsum"
required-features = ["std", "clap"]

[features]
default = ["std", "digest-trait"]
std = ["digest?/std"]
digest-trait = ["digest"]
oid = ["const-oid"]
